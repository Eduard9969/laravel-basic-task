<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Admin Routes
 */
Route::group(['prefix' => 'admin'], function() {

    /*
     * Admin User Control Panel Routes
     */
    Route::resource('users', 'Admin\Auth\ControlPanelController')->names([
        'index'     => 'admin.users',
        'create'    => 'admin.users.create',
        'store'     => 'admin.users.store',
        'show'      => 'admin.users.show',
        'edit'      => 'admin.users.edit',
        'update'    => 'admin.users.update',
        'destroy'   => 'admin.users.destroy',
    ]);
});

/*
 * Home Route
 */
Route::get('/', 'Main\MainController@home')->name('home');

/*
 * Dashboard Route
 */
Route::group(['prefix' => 'dashboard'], function() {
    Route::get('users', 'Main\DashboardController@users')->name('dashboard.users');
});

/*
 * Auth Page
 */
Route::group(['prefix' => 'auth'], function() {
    Route::get('login', 'Auth\LoginController@login')->name('login');
    Route::post('login', 'Auth\LoginController@loginHandler')->name('loginHandler');

    Route::get('logout', 'Auth\LogoutController@logout')->name('logout');

    Route::get('register', 'Auth\RegisterController@register')->name('register');
    Route::post('register', 'Auth\RegisterController@registerHandler')->name('registerHandler');
});
