<?php

return [

    /*
     * The number of elements per page
     */
    'per_page' => 25,

    /*
     * The number of entries to display to users without privileges
     */
    'count_basic_elements' => 10

];
