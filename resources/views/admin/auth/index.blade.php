@extends('layouts.main')

@section('content')
    <div class="col-12 mt-3"></div>

    @role('admin')
        <div class="row">
            <div class="col-12 mt-2">
                <a href="{{ route('admin.users.create') }}" class="btn btn-success float-right">
                    @lang('users.add_user')
                </a>
            </div>
        </div>
    @endrole
    <div class="row">
        <div class="col-12 mt-2">
            @if(!empty($users))
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="border-top-0">@lang('users.list.id')</th>
                            <th class="border-top-0">@lang('users.list.name')</th>
                            <th class="border-top-0">@lang('users.list.email')</th>
                            <th class="border-top-0">@lang('users.list.role')</th>
                            <th class="border-top-0"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @forelse($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if(!empty($user->roles))
                                        @foreach($user->roles as $user_role)
                                            {{ $user_role->name }} @if($loop->index > 0), @endif
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin.users.show', ['user' => $user->id]) }}" title="@lang('users.show_user') {{ $user->name }}" >
                                        @lang('users.show_user')
                                    </a>
                                    <a href="{{ route('admin.users.edit', ['user' => $user->id]) }}" title="@lang('users.edit_user') {{ $user->name }}" >
                                        @lang('users.edit_user')
                                    </a>
                                    <a href="{{ route('admin.users.destroy', ['user' => $user->id]) }}" title="@lang('users.delete_user') {{ $user->name }}" >
                                        @lang('users.delete_user')
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" align="center">
                                    @lang('pagination.empty')
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

                @if($users instanceof \Illuminate\Pagination\AbstractPaginator)
                    {{ $users->links() }}
                @endif
            @endif
        </div>
    </div>
@endsection
