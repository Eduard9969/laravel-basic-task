<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">

        <title>{{ (isset($title) ? $title : 'Default') }}</title>

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="{{ asset('dist/css/critical.css') }}">
    </head>

    <body>

        <div class="notification_area position-fixed">
            @if($errors->any())
                <div class="box-color bg-danger position-relative pointer" onclick="this.remove()">
                    <span class="arrow right bg-danger"></span>
                    <div class="box-body py-2 px-3 text-white">
                        {{$errors->first()}}
                    </div>
                </div>
            @endif

            @if ($session::has('message') && !$errors->any())
                <div class="box-color bg-success position-relative pointer" onclick="this.remove()">
                    <span class="arrow right bg-success"></span>
                    <div class="box-body py-2 px-3 text-white">
                        {{ $session::get('message') }}
                    </div>
                </div>
                {{ $session::remove('message') }}
            @endif
        </div>

        <div class="container-lg m-auto">
            @yield('content')
        </div>

        <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}">
        <script src="{{ asset('dist/js/app.js') }}"></script>

    </body>
</html>
