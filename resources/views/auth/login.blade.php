@extends('layouts.main')

@section('content')
    <div class="mt-5">
        <div class="row">
            <div class="col-6 offset-3">
                <form action="{{ route('loginHandler') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="emailorlogin">{{ __('auth.emailorlogin') }}</label>
                        <input
                            class="form-control"
                            type="text"
                            name="emailorlogin"
                            id="emailorlogin"
                            value="{{ old('emailorlogin') }}"
                            autocomplete="false"
                            required
                            placeholder="{{ __('auth.emailorlogin') }}" >
                    </div>

                    <div class="form-group">
                        <label for="pass">{{ __('auth.password') }}</label>
                        <input
                            class="form-control"
                            type="password"
                            name="password"
                            id="pass"
                            value="{{ old('password') }}"
                            autocomplete="false"
                            required
                            placeholder="{{ __('auth.password') }}" >
                    </div>

                    <div class="form-check">
                        <input
                            class="form-check-input"
                            type="checkbox"
                            name="remember"
                            @if((bool) old('remember')) checked="checked" @endif
                            id="remember">
                        <label class="form-check-label" for="remember">{{ __('auth.remember') }}</label>
                    </div>

                    <div class="mt-3">
                        <input class="btn btn-outline-primary" type="submit" value="{{ __('auth.submit') }}">
                        <p class="small mt-2 mb-0">
                            <a href="{{ route('register') }}" title="{{ __('auth.forgot_password') }}">{{ __('auth.forgot_password') }}</a>
                        </p>
                        <p class="small">
                            {{ __('auth.question_req') }} -
                            <a href="{{ route('register') }}" title="{{ __('auth.answer_req_now') }}">{{ __('auth.answer_req_now') }}</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
