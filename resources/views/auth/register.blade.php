@extends('layouts.main')

@section('content')
    <div class="mt-5">
        <div class="row">
            <div class="col-6 offset-3">
                <form action="{{ route('registerHandler') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="email">{{ __('auth.email') }}</label>
                        <input
                            class="form-control"
                            type="text"
                            name="email"
                            id="email"
                            value="{{ old('email') }}"
                            autocomplete="false"
                            required
                            placeholder="{{ __('auth.email') }}" >
                    </div>

                    <div class="form-group">
                        <label for="login">{{ __('auth.login') }}</label>
                        <input
                            class="form-control"
                            type="text"
                            name="login"
                            id="login"
                            value="{{ old('login') }}"
                            autocomplete="false"
                            required
                            placeholder="{{ __('auth.login') }}" >
                    </div>

                    <div class="form-group">
                        <label for="pass">{{ __('auth.password') }}</label>
                        <input
                            class="form-control"
                            type="password"
                            name="password"
                            id="pass"
                            value="{{ old('password') }}"
                            autocomplete="false"
                            required
                            placeholder="{{ __('auth.password') }}" >
                    </div>

                    <div class="form-group">
                        <label for="pass_again">{{ __('auth.password_again') }}</label>
                        <input
                            class="form-control"
                            type="password"
                            name="password_confirmation"
                            id="pass_again"
                            value="{{ old('password_confirmation') }}"
                            autocomplete="false"
                            required
                            placeholder="{{ __('auth.password_again') }}" >
                    </div>

                    <div class="mt-3">
                        <input class="btn btn-outline-primary" type="submit" value="{{ __('auth.answer_req_now') }}">
                        <p class="small mt-2 mb-0">
                            {{ __('auth.question_auth') }} -
                            <a href="{{ route('login') }}" title="{{ __('auth.submit') }}">{{ __('auth.submit') }}</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
