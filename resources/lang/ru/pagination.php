<?php

return [
    'previous' => '&laquo; Предыдущая',
    'next' => 'Следующая &raquo;',

    'empty' => 'Нет записей',
];
