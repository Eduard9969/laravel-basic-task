<?php

return [
    'add_user' => 'Добавить пользователя',

    'list' => [
        'id'    => 'ID',
        'name'  => 'Имя',
        'email' => 'Email',
        'role'  => 'Роли'
    ],

    'show_user'   => 'Смотреть',
    'edit_user'   => 'Редактировать',
    'delete_user' => 'Удалить',

    'error' => [
        'delete' => 'Ошибка удаления пользователя',
    ],
];
