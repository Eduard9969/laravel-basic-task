<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'emailorlogin'      => 'Email or Login',
    'email'             => 'Email',
    'login'             => 'Login',
    'password'          => 'Password',
    'password_again'    => 'Password again',
    'remember'          => 'To remember me',
    'submit'            => 'Login',

    'required_email'    => 'Email or login required!',
    'required_pass'     => 'Password is required!',

    'confirmed_pass'    => 'Re-password does not match',

    'string_email'      => 'Email is string!',
    'string_login'      => 'Login is string!',
    'string_pass'       => 'Password is string!',

    'not_valid_email'   => 'Invalid email or login',
    'not_valid_pass'    => 'Invalid password',

    'max_login_lenght'  => 'Login exceeds maximum length',
    'max_email_lenght'  => 'Email exceeds maximum length',

    'min_pass_lenght'   => 'Password cannot be shorter than 4 characters',

    'not_valid_user'    => 'User with such data was not found.',

    'has_login'         => 'This Login is already registered',
    'has_email'         => 'This Email is already registered',

    'forgot_password'   => 'Forgot your password?',

    'question_req'      => 'Don\'t have an account?',
    'answer_req_now'    => 'Register now',

    'failed'            => 'These credentials do not match our records.',
    'throttle'          => 'Too many login attempts. Please try again in :seconds seconds.',

    'question_auth'     => 'You have an account?',
];
