<?php

return [
    'add_user' => 'Add User',

    'list'  => [
        'id'    => 'Identifier',
        'name'  => 'Name',
        'email' => 'Email',
        'role'  => 'Role`s'
    ],

    'show_user'   => 'Show',
    'edit_user'   => 'Edit',
    'delete_user' => 'Delete',

    'error' => [
        'delete' => 'Error deleting user',
    ],
];
