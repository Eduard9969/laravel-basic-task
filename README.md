![Alt text](https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg)

## О репозитории

**Тестовое задание:**

Написать панель управления пользователями, с регистрацией и авторизацией + CRUD.

- В базу данных добавить 100000 записей, каждая запись должна содержать 10 аттрибутов, 3 из 10 аттрибутов должны быть в связных таблицах.
- Все записи вывести списком в панель управления администратора. 
- 10 первых записей (по порядку), вывести в панель пользователя (личный кабинет).
- Обязательно использование Laravel, jQuery, Bootstrap css

## About Repository

**Test Task:**

Write a user control panel, with registration and authorization + CRUD.

- Add 100,000 records to the database, each record should contain 10 attributes, 3 out of 10 attributes should be in linked tables.
- All entries are listed in the admin control panel.
- 10 first entries (in order), display in the user panel (personal account).
- Mandatory use of Laravel, jQuery, Bootstrap css
