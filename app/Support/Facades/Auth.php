<?php

namespace App\Support\Facades;

use \Illuminate\Support\Facades\Auth as Authentication;

/**
 * Class Auth
 * @package App\Support\Facades
 */
class Auth extends Authentication
{
    ///
}
