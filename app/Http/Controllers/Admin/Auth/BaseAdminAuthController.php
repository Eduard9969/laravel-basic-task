<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Admin\BaseAdminController;

/**
 * Class BaseAdminAuthController
 * @package App\Http\Controllers\Admin\Auth
 */
class BaseAdminAuthController extends BaseAdminController
{

    /**
     * BaseAdminAuthController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
