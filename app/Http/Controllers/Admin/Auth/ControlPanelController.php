<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Models\Auth\User;
use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Http\Request;

class ControlPanelController extends BaseAdminAuthController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::paginate(config('pagination.per_page'));

        $this->_assign('users', $users);
        return view('admin.auth.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.auth.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RegisterRequest $request
     * @return void
     */
    public function store(RegisterRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::find($id);

        $this->_assign('user', $user);
        return view('admin.auth.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::find($id);

        $this->_assign('user', $user);
        return view('admin.auth.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $user    = User::find($id);
        $deleted = $user->delete();

        if(!$deleted)
            return redirect()->back()->withErrors([trans('users.error.delete')]);

        $user->detachAllRoles();

        return redirect()->to(route('admin.users.index'));
    }
}
