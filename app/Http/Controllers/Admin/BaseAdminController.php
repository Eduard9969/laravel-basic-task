<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

/**
 * Class BaseAdminController
 * @package App\Http\Controllers\Admin
 */
class BaseAdminController extends Controller
{

    /**
     * BaseAdminController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth.role:admin');
    }

}
