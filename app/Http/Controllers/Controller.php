<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests,
        HasRoleAndPermission;

    /**
     * @var int|null current user id
     */
    protected $user_id = null;

    /**
     * @var |null current user login
     */
    protected $user_login = null;

    /**
     * @var bool current user is admin
     */
    protected $user_is_admin = false;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->setGlobalView();
        $this->middleware(function ($request, $next) {
            $this->user_id       = (int) Auth::id();
            $this->user_login    = !empty(Auth::user()->login) ? Auth::user()->login : null;

            if(Auth::check())
                $this->user_is_admin = Auth::user()->isAdmin();

            return $next($request);
        });
    }

    /**
     * Global view variables
     *
     * @return bool
     */
    private function setGlobalView() : bool
    {
        self::setViewAssign('session', Session::class); // Session Object

        self::setViewAssign('this_user_id', $this->user_id);
        self::setViewAssign('this_user_login', $this->user_login);
        self::setViewAssign('this_user_is_admin', $this->user_is_admin);

        return true;
    }

    /**
     * A simple form of assigning a view variable
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    private static function setViewAssign($key, $value)
    {
        return View::share($key, $value);
    }

    /**
     * Assigning a variable to the template body
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    protected function _assign($key, $value)
    {
        return self::setViewAssign($key, $value);
    }
}
