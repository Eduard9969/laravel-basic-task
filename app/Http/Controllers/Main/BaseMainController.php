<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class BaseMainController
 * @package App\Http\Controllers\Main
 */
class BaseMainController extends Controller
{

    /**
     * BaseMainController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
