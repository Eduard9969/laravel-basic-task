<?php

namespace App\Http\Controllers\Main;

use App\Support\Facades\Auth;

/**
 * Class MainController
 * @package App\Http\Controllers\Main
 */
class MainController extends BaseMainController
{

    /**
     * Home Method
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('welcome');
    }
}
