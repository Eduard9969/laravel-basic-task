<?php

namespace App\Http\Controllers\Main;

use App\Http\Models\Auth\User;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Main
 */
class DashboardController extends BaseMainController
{

    /**
     * User Method
     */
    public function users()
    {
        $users = User::limit(config('pagination.count_basic_elements'))->get();

        $this->_assign('users', $users);
        return view('admin.auth.index');
    }
}
