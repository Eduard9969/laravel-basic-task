<?php

namespace App\Http\Controllers\Auth;

use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;

/**
 * Class LogoutController
 * @package App\Http\Controllers\Auth
 */
class LogoutController extends BaseAuthController
{

    /**
     * Where to redirect users after logout.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * LogoutController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Method LogOut
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        if(Auth::check())
            Auth::logout();

        return redirect()->to($this->redirectTo);
    }
}
