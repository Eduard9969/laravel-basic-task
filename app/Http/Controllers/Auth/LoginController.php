<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');
    }

    /**
     * Login Method
     */
    public function login()
    {
        return view('auth.login');
    }

    /**
     * Login Handler
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loginHandler(LoginRequest $request)
    {
        $email = $request->post('email');
        $login = $request->post('login');
        $pass  = $request->post('password');
        $rmmbr = (bool) $request->post('remember', false);

        $key_name   = (!empty($email) ? 'email' : 'login');
        $value_name = (!empty($email) ? $email : $login);

        $auth[$key_name]  = $value_name;
        $auth['password'] = $pass;

        if(Auth::attempt($auth, $rmmbr))
            return redirect()->intended($this->redirectTo);
        else
            return redirect()->route('login')->withErrors([trans('auth.not_valid_user')]);

    }

}
