<?php


namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

/**
 * Class BaseAuthController
 * @package App\Http\Controllers\Auth
 */
class BaseAuthController extends Controller
{

    /**
     * BaseAuthController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

}
