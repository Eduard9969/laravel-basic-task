<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfRole
{

    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @var string redirect to
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * RedirectIfRole constructor.
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if ($this->auth->check() && $this->auth->user()->hasRole($role)) {
            return $next($request);
        }

        return redirect($this->redirectTo);
    }
}
