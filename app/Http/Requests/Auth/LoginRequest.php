<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator as Validator;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Patch Validator Instance
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();

        $field_name = 'emailorlogin';
        if (isset($data[$field_name]))
        {
            $validate = Validator::make($data, [
                $field_name => 'required|min:5|max:255'
            ]);

            if(!$validate->fails())
            {
                $validate = Validator::make($data, [
                    $field_name => 'email'
                ]);

                if($validate->fails())
                {
                    $data['login'] = $data[$field_name];
                }
                else {
                    $data['email'] = $data[$field_name];
                }

                unset($data[$field_name]);
            }
        }

        $this->getInputSource()->replace($data);

        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['password'] = 'required|min:5|max:255';

        $data = $this->all();
        if(isset($data['email']))
            $rules['email'] = 'required|email|min:5';
        else
            $rules['login'] = 'required|min:3|max:255';

        return $rules;
    }

    /**
     * Custom message text
     *
     * @return array
     */
    public function messages()
    {
        $required_email  = trans('auth.required_email');
        $required_pass   = trans('auth.required_pass');

        $not_valid_email = trans('auth.not_valid_email');
        $not_valid_pass  = trans('auth.not_valid_pass');

        return [
            'email.required'    => $required_email,
            'email.email'       => $not_valid_email,
            'email.min'         => $not_valid_email,

            'login.required'    => $required_email,
            'login.min'         => $not_valid_email,
            'login.max'         => $not_valid_email,

            'password.required' => $required_pass,
            'password.min'      => $not_valid_pass,
            'password.max'      => $not_valid_pass,
        ];
    }
}
