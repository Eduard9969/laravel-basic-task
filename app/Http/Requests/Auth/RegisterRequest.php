<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login'     => 'required|string|max:255|unique:users',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:8|confirmed',
        ];
    }

    /**
     * Custom message text
     *
     * @return array
     */
    public function messages()
    {
        return [
            'login.required'        => trans('auth.required_email'),
            'login.string'          => trans('auth.string_login'),
            'login.max'             => trans('auth.max_login_lenght'),
            'login.unique'          => trans('auth.has_login'),

            'email.required'        => trans('auth.required_email'),
            'email.string'          => trans('auth.string_email'),
            'email.email'           => trans('auth.not_valid_email'),
            'email.max'             => trans('auth.max_email_lenght'),
            'email.unique'          => trans('auth.has_email'),

            'password.required'     => trans('auth.required_pass'),
            'password.string'       => trans('auth.string_pass'),
            'password.min'          => trans('auth.min_pass_lenght'),
            'password.confirmed'    => trans('auth.confirmed_pass'),
        ];
    }
}
